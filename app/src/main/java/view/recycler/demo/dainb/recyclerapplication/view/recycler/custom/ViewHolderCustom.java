package view.recycler.demo.dainb.recyclerapplication.view.recycler.custom;

import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import view.recycler.demo.dainb.recyclerapplication.R;

/**
 * Created by dainb on 4/26/16.
 */
public class ViewHolderCustom extends RecyclerView.ViewHolder {
    private static final String TAG = "ViewHolderCustom";
    private final TextView textView;

    public ViewHolderCustom(View itemView) {
        super(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Element " + getLayoutPosition() + " clicked.",
                        Snackbar.LENGTH_SHORT).setAction("Action", null).show();
            }
        });
        textView = (TextView) itemView.findViewById(R.id.textView);

        if ((getLayoutPosition() % 2) == 0) this.itemView.setBackgroundColor(Color.GRAY);
        else this.itemView.setBackgroundColor(Color.WHITE);

    }

    public TextView getTextView() {
        return textView;
    }
}
